FROM openshift/base-centos7

MAINTAINER https://gitlab.cern.ch/hse-see-co-docker/openshift-java-s2i

EXPOSE 8080

ENV MAVEN_VERSION=3.3.9 \
    GRADLE_VERSION=5.6.2 \
    APN_VERSION=1.2.31

LABEL io.k8s.description="Platform for building and running Java Applications" \
      io.k8s.display-name="Java 8 (Maven/Gradle)" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,java,java8,maven,gradle,springboot" \
      io.openshift.s2i.destination="/opt/s2i/destination"
ADD CentOS-CERN.repo /etc/yum.repos.d/

# Install deps
RUN INSTALL_PKGS="tar unzip bc which lsof java-1.8.0-openjdk java-1.8.0-openjdk-devel CERN-CA-certs apr-devel openssl-devel cern-get-sso-cookie  krb5-workstation" && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y
ENV HOME=${HOME:-/opt/app-root/src}
ENV JAVA_HOME=/etc/alternatives/java_sdk

WORKDIR $HOME

# Install Maven
RUN (curl -v https://downloads.apache.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | \
    tar -zx -C /usr/local) && \
    ln -sf /usr/local/apache-maven-$MAVEN_VERSION/bin/mvn /usr/local/bin/mvn && \
    mkdir -p $HOME/.m2


#Install Gradle
RUN curl -sL -0 https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -o /tmp/gradle-${GRADLE_VERSION}-bin.zip && \
    unzip /tmp/gradle-${GRADLE_VERSION}-bin.zip -d /usr/local/ && \
    rm /tmp/gradle-${GRADLE_VERSION}-bin.zip && \
    mv /usr/local/gradle-${GRADLE_VERSION} /usr/local/gradle && \
    ln -sf /usr/local/gradle/bin/gradle /usr/local/bin/gradle

# Make dir for s2i
RUN mkdir -p /opt/s2i/destination && mkdir -p $HOME/.ssh

RUN curl https://downloads.apache.org/tomcat/tomcat-connectors/native/${APN_VERSION}/source/tomcat-native-${APN_VERSION}-src.tar.gz -o tomcat-native.tar.gz && \
    tar -zxvf tomcat-native.tar.gz && \
    cd tomcat-native-${APN_VERSION}-src/native && chmod +x configure && \
    ./configure --with-apr=/usr/bin/apr-1-config \
            --with-java-home=${JAVA_HOME} \
            --with-ssl=yes \
            --prefix=${HOME}/apache-tomcat && \
    make && make install

ADD ./settings/settings.xml $HOME/.m2/
ADD ./settings/init.gradle $HOME/.gradle/
ADD ./settings/.ssh/config $HOME/.ssh/config
#ADD ./contrib/tnsnames.ora /etc/

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

RUN chown -R 1001:0 $HOME && \
    chmod -R ug+rw /opt/s2i/destination && \
    chmod -R ug+r $HOME/.ssh && \
    chmod -R ug+rwx $STI_SCRIPTS_PATH && \
    chown -R 1001:1001 /opt/app-root

RUN keytool -import -noprompt -trustcacerts -alias "CERN Root Certification Authority 2" -file "/etc/pki/tls/certs/CERN_Root_Certification_Authority_2.crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority" -file "/etc/pki/tls/certs/CERN_Certification_Authority.crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit && \
    keytool -import -noprompt -trustcacerts -alias "CERN Certification Authority(1)" -file "/etc/pki/tls/certs/CERN_Certification_Authority(1).crt" -keystore "$JAVA_HOME/jre/lib/security/cacerts" --storepass changeit

ADD krb5.conf /etc/krb5.conf
RUN chmod +r /etc/krb5.conf
USER 1001

CMD $STI_SCRIPTS_PATH/usage
